﻿using ControllersAndActionMethods.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ControllersAndActionMethods.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            ViewData["userName"] = User.Identity.Name;
            ViewData["serverName"] = Server.MachineName;
            ViewData["clientIP"] = Request.UserHostAddress;
            ViewData["dateStamp"] = HttpContext.Timestamp;
            ViewData["cookieCount"] = Request.Cookies.Count;
            return View();
        }

        // Parameter and model binding
        public string Gimme(int id)
        {
            return string.Format("Success? id = {0}", id);
        }

        public string GimmeMaybe(int? id)
        {
            return string.Format("Success? id = {0}", id);
        }

        public string GimmeAPerson(Person person)
        {
            return "?";
        }

        // Selectively bind properties
        public ActionResult Update([Bind(Exclude = "PersonID")] Person person)
        {
            return View();
        }

        /*
        public ActionResult Create([Bind(Include = "PersonID,FullName,PreferredName,SearchName,IsPermittedToLogon,LogonName,IsExternalLogonProvider,HashedPassword,IsSystemUser,IsEmployee,IsSalesperson,UserPreferences,PhoneNumber,FaxNumber,EmailAddress,Photo,CustomFields,OtherLanguages,LastEditedBy,ValidFrom,ValidTo")] Person person)
        {
        }
        */

        // Returning ActionResults

        // Status code
        public ActionResult RandomStatusCode()
        {
            Random gen = new Random();
            int statusCode = gen.Next(400, 500);
            return new HttpStatusCodeResult(statusCode);
        }

        // Redirects
        public ActionResult RedirectMe()
        {
            return RedirectToAction("Index");
        }

        public ActionResult News()
        {
            return Redirect("https://www.latlmes.com/entertainment/new-game-of-thrones-trailer-reveals-final-season-will-be-cobbled-together-from-old-footage-1");
        }
    }
}