﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AJAX_Example.Controllers
{
    public class SimpleAPIController : Controller
    {
        // GET: SimpleAPI/RandomNumbers/10
        public JsonResult RandomNumbers(int? id = 100)
        {
            Random gen = new Random();

            var data = new
            {
                message = "Hello from SimpleAPI",
                num = (int)id,
                numbers1 = Enumerable.Range(1,(int)id),
                numbers = Enumerable.Range(1, (int)id).Select(x => gen.Next(1000))
            };
            return Json(data,JsonRequestBehavior.AllowGet );
        }
    }
}