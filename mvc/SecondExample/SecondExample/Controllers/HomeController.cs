﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SecondExample.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        // Controller Action Method
        [HttpGet]
        public ActionResult Message()
        {
            // I want the query string values
            Debug.WriteLine(Request.QueryString["user_name"]);
            Debug.WriteLine(Request.QueryString["user_email"]);
            Debug.WriteLine(Request.QueryString["user_message"]);

            string name = Request.QueryString["user_name"];
            if(name != null)
            {
                string message = "Hello " + name + "! Welcome.";
                ViewBag.message = message;
                // Same as 
                //ViewData["message"] = message;
            }
            
            return View();
        }

        public ActionResult Message2()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Message2(string user_name, string user_email, string user_message)
        {
            Debug.WriteLine(user_name);
            Debug.WriteLine(user_email);
            Debug.WriteLine(user_message);
            return View();
        }


    }
}